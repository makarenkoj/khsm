class GamesController < ApplicationController
  before_action :authenticate_user!

  before_action :goto_game_in_progress!, only: [:create]

  before_action :set_game, except: [:create]

  before_action :redirect_from_finished_game!, except: [:create]

  def show
    @game_question = @game.current_game_question
  end

  def create
    begin
      @game = Game.create_game_for_user!(current_user)

      redirect_to game_path(@game), notice: "Игра началась в %{created_at}, время пошло!"
    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotSaved => ex
      Rails.logger.error("Error creating game for user #{current_user.id}, msg = #{ex}. #{ex.backtrace}")
      redirect_to :back, alert: "Не удалось создать игру :( Свяжитесь админом ресурса"
    end
  end

  def answer
    @answer_is_correct = @game.answer_current_question!(params[:letter])
    @game_question = @game.current_game_question

    unless @answer_is_correct
      flash[:alert] = "Правильный ответ: %{answer}. Игра закончена  :(....."
    end

    respond_to do |format|
      format.html do
        if @answer_is_correct && !@game.finished?
          redirect_to game_path(@game)
        else
          redirect_to user_path(current_user)
        end
      end

      format.ja {}
    end
  end

  def take_money
    @game.take_money!
    redirect_to user_path(current_user),
                flash: {warning: "Игра оконченаб ваш выигрыш %{prize} . Заходите ещё!"}
  end

  def help
    msg = if @game.use_help(params[:help_type].to_sym)
            {flash: {info: "Вы использовали подсказку"}}
          else
            {alert: "Подсказка уже была использована!"}
          end

    redirect_to game_path(@game), msg
  end

  private

  def redirect_from_finished_game!
    redirect_to user_path(current_user), alert: "Игра %{game_id} закончена и больше недоступна, на...."
  end

  def goto_game_in_progress!
    game_in_progress = current_user.games.in_progress.first
    redirect_to game_path(game_in_progress), alert: "Вы ещё не завершили  игру!"
  end

  def set_game
    @game = current_user.games.find_by(id: params[:id])
    redirect_to root_path, alert: "Это не ваша игра!" if @game.blank?
  end
end
